# ARSO weather module

You can find service documentation here: http://meteo.arso.gov.si/uploads/meteo/help/sl/xml_service.pdf

Some shortcut examples used in ARSO service:
- FZRA - Freezing rain
- SHRA - Shower of rain
- SHGR - Shower of graupel
- TSRA - Thunderstorm with rain
- TSSN - Thunderstorm with snow

## DRUPAL 8 UI
1.) Go to admin/structure/block
2.) Place block and search for Weather Forecast.


## Composer
1.) In order for this block to works as it should, you have to add external HTML DOM library to your project using composer.
Run `composer require kub-at/php-simple-html-dom-parser` in you drupal root to download Simple HTML DOM library to vendor folder.

## Compile CSS
1.) Go to weather module and run `npm install`

2.) Run `npm gulp watch` if you want to compile css from SASS.

## Folders
- dist -> Compiled CSS
- scss -> SASS files
- src -> Plugin Block
- assets -> Images
- templates -> Twig templates
