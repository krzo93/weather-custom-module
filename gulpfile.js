'use strict';

const gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  cleanCSS = require('gulp-clean-css'),
  sourcemaps = require('gulp-sourcemaps'),
  rename = require("gulp-rename");

sass.compiler = require('node-sass');

const css = exports.css = function css() {
  return gulp.src('./scss/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({}).on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(cleanCSS())
  .pipe(rename('weather.css'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('./dist'));
};

const watch = exports.watch = function watch() {
  gulp.watch('./scss/*.scss', css);
};

exports.default = gulp.series(gulp.parallel(css));
