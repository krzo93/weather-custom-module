<?php

namespace Drupal\weather\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use SimpleXMLElement;
use Drupal\file\Entity\File;
use KubAT\PhpSimple\HtmlDomParser;

/**
 * Provides a 'WeatherForecast' block.
 *
 * @Block(
 *  id = "weather_forecast",
 *  admin_label = @Translation("Weather Forecast"),
 * )
 */
class WeatherForecast extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\State\StateInterface definition.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->state = $container->get('state');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form_state->disableCache();

    $city = $this->getCities();
    $region = $this->getRegions();
    $hidro_location = $this->getHidroLocation();
    $snow_locations = $this->getSnowLocations();

    $form['weather_city'] = [
      '#type' => 'select',
      '#title' => 'Select city',
      '#description' => 'Današnja napoved za kraj.',
      '#options' => $city,
      '#default_value' => isset($config['weather_city']) ? $config['weather_city'] : FALSE,
      '#required' => TRUE,
    ];

    $form['weather_region'] = [
      '#type' => 'select',
      '#title' => 'Select region',
      '#description' => 'Napoved vremena za 3 dni - Regija',
      '#options' => $region,
      '#default_value' => isset($config['weather_region']) ? $config['weather_region'] : FALSE,
      '#required' => TRUE,
    ];

    $form['weather_footer_fieldset'] = [
      '#type' => 'details',
      '#title' => t('Footer settings'),
    ];

    $form['weather_footer_fieldset']['weather_footer_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Footer text'),
      '#placeholder' => $this->t('Footer text'),
      '#default_value' => isset($config['weather_footer_text']) ? $config['weather_footer_text'] : FALSE,
    ];

    $form['weather_footer_fieldset']['weather_footer_data_options'] = [
      '#type' => 'select',
      '#title' => 'Select data you would like to show in footer',
      '#options' => [
        'empty' => '- None -',
        'water' => 'Temperature vode',
        'snow' => 'Višina snežne odeje',
        'temperature' => 'Temperatura kraja',
      ],
      '#default_value' => isset($config['weather_footer_data_options']) ? $config['weather_footer_data_options'] : FALSE,
    ];

    $form['weather_footer_fieldset']['weather_footer_hidro'] = [
      '#type' => 'select',
      '#title' => 'Select hidro station',
      '#options' => $hidro_location,
      '#default_value' => isset($config['weather_footer_hidro']) ? $config['weather_footer_hidro'] : FALSE,
      '#states' => [
        'visible' => [
          'select[name="settings[weather_footer_fieldset][weather_footer_data_options]"]' => ['value' => 'water'],
        ],
        'required' => [
          'select[name="settings[weather_footer_fieldset][weather_footer_data_options]"]' => ['value' => 'water'],
        ],
      ],
    ];

    $form['weather_footer_fieldset']['weather_footer_temperature'] = [
      '#type' => 'select',
      '#title' => 'Select city temperature',
      '#options' => $city,
      '#default_value' => isset($config['weather_footer_temperature']) ? $config['weather_footer_temperature'] : FALSE,
      '#states' => [
        'visible' => [
          'select[name="settings[weather_footer_fieldset][weather_footer_data_options]"]' => ['value' => 'temperature'],
        ],
        'required' => [
          'select[name="settings[weather_footer_fieldset][weather_footer_data_options]"]' => ['value' => 'temperature'],
        ],
      ],
    ];

    $form['weather_footer_fieldset']['weather_footer_snow'] = [
      '#type' => 'select',
      '#title' => 'Select city for snow conditions',
      '#options' => $snow_locations,
      '#default_value' => isset($config['weather_footer_snow']) ? $config['weather_footer_snow'] : FALSE,
      '#states' => [
        'visible' => [
          'select[name="settings[weather_footer_fieldset][weather_footer_data_options]"]' => ['value' => 'snow'],
        ],
        'required' => [
          'select[name="settings[weather_footer_fieldset][weather_footer_data_options]"]' => ['value' => 'snow'],
        ],
      ],
    ];

    $form['weather_footer_fieldset']['image'] = [
      '#type' => 'managed_file',
      '#title' => t('Logotip'),
      '#theme' => 'image_widget',
      '#upload_location' => 'public://weather/',
      '#default_value' => isset($config['image']) ? $config['image'] : FALSE,
    ];

    $form['weather_footer_fieldset']['image_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Logotip URL'),
      '#default_value' => isset($config['image_url']) ? $config['image_url'] : FALSE,
      '#maxlength' => 255,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['weather_region'] = $values['weather_region'];
    $this->configuration['weather_city'] = $values['weather_city'];
    $this->configuration['weather_footer_text'] = $values["weather_footer_fieldset"]['weather_footer_text'];
    $this->configuration['weather_footer_data_options'] = $values["weather_footer_fieldset"]['weather_footer_data_options'];
    $this->configuration['weather_footer_icon'] = $values["weather_footer_fieldset"]['weather_footer_icon'];
    $this->configuration['image_url'] = $values["weather_footer_fieldset"]['image_url'];
    $this->configuration['weather_footer_temperature'] = $values["weather_footer_fieldset"]['weather_footer_temperature'];
    $this->configuration['weather_footer_hidro'] = $values["weather_footer_fieldset"]['weather_footer_hidro'];
    $this->configuration['weather_footer_snow'] = $values["weather_footer_fieldset"]['weather_footer_snow'];

    // Set logotip image status to permenant.
    $image = $values["weather_footer_fieldset"]["image"];
    if (!empty($image)) {
      $file = File::load($image[0]);
      $file->setPermanent();
      $file->save();
    }
    $this->configuration['image'] = $values["weather_footer_fieldset"]["image"];
  }

  /**
   * Function returns array of values, we get from ARSO response.
   *
   * @param $response
   *
   * @return array
   */
  private function parseResponse($response) {
    $data = [];
    if (!is_null($response) && $response !== FALSE) {
      $xml = new SimpleXMLElement($response, NULL, FALSE);
      if (sizeof($xml->metData) > 0) {
        foreach ($xml->metData as $item) {
          $data[] = [
            'domain_title' => (string) $item->domain_title,
            'domain_lat' => (string) $item->domain_lat,
            'domain_lon' => (string) $item->domain_lon,
            'domain_altitude' => (string) $item->domain_altitude,
            'valid_day' => (string) $item->valid_day,
            'valid_date' => (string) $item->valid,
            'temperature' => [
              't_var_desc' => (string) $item->t_var_desc,
              't_var_unit' => (string) $item->t_var_unit,
              't' => (string) $item->t,
              'tnsyn_var_desc' => (string) $item->tnsyn_var_desc,
              'tnsyn_var_unit' => (string) $item->tnsyn_var_unit,
              'tnsyn' => (string) $item->tnsyn,
              'txsyn_var_desc' => (string) $item->txsyn_var_desc,
              'txsyn_var_unit' => (string) $item->txsyn_var_unit,
              'txsyn' => (string) $item->txsyn,
            ],
            'humidity' => [
              'rh_var_desc' => (string) $item->rh_var_desc,
              'rh_var_unit' => (string) $item->rh_var_unit,
              'rh' => (string) $item->rh,
            ],
            'snow' => [
              'snow_var_desc' => (string) $item->snow_var_desc,
              'snow_var_unit' => (string) $item->snow_var_unit,
              'snow' => (string) $item->snow,
            ],
            'water_temperature' => [
              'snow_var_desc' => (string) $item->tw_var_desc,
              'snow_var_unit' => (string) $item->tw_var_unit,
              'snow' => (string) $item->tw,
            ],
            'weather_icon' => [
              'nn_icon' => $item->nn_icon,
              'nn_shortText' => $item->nn_shortText,
              'nn_decodeText' => $item->nn_decodeText,
              'wwsyn_icon' => $item->wwsyn_icon,
              'wwsyn_shortText' => $item->wwsyn_shortText,
              'wwsyn_longText' => $item->wwsyn_longText,
              'wwsyn_decodeText' => $item->wwsyn_decodeText,
              'rr_icon' => $item->rr_icon,
              'rr_decodeText' => $item->rr_decodeText,
              'ts_icon' => $item->ts_icon,
              'fog_icon' => $item->fog_icon,
              'nn_icon-wwsyn_icon' => $item->{"nn_icon-wwsyn_icon"},
            ],
          ];
        }
      }

    }
    return $data;
  }

  /**
   * Get all data for configuration form.
   *
   * @param $response
   *
   * @return array
   */
  private function parseLocation($response) {
    $location = [];

    // Check if response !is_null.
    if (!is_null($response) && $response !== FALSE) {
      $xml = new SimpleXMLElement($response, NULL, FALSE);

      // Hidro location.
      if (isset($xml->postaja)) {
        // Loop over all data.
        foreach ($xml->postaja as $hidro_location) {
          // Cast SimpleXMLElement to string.
          $hidro_name = (string) $hidro_location->merilno_mesto;

          // Check if hidro_name already in array.
          if (!in_array($hidro_name, $location)) {
            $location[$hidro_name] = $hidro_name;
          }
        }
      }
      elseif (sizeof($xml->metData) > 0) {
        // Loop over all data.
        foreach ($xml->metData as $region) {
          // Cast SimpleXMLElement to string.
          $domain_id = (string) $region->domain_meteosiId;
          $domain_name = (string) $region->domain_title;

          // Check if region already in array.
          if (!in_array($domain_id, $location)) {
            $location[$domain_id] = $domain_name;
          }
        }
      }
    }

    ksort($location);

    return $location;
  }

  /**
   * Function for API call.
   *
   * @param $url
   *
   * @return bool|string
   */
  private function sendUrlRequest($url) {
    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 20);

    // Get the HTML or whatever is linked in $url.
    $response = curl_exec($handle);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    if ($httpCode == 404) {
      return NULL;
    }

    curl_close($handle);

    return $response;
  }

  /**
   * Choose city.
   */
  function currentWeather($city_id) {
    $url = 'https://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observationAms_' . $city_id . 'latest.xml';
    $request = $this->sendUrlRequest($url);

    if ($request !== FALSE) {
      return $this->parseResponse($request);
    }

    return FALSE;
  }

  /**
   * Choose region.
   */
  function extendedWeatherForecast($region_id) {
    $url = 'https://meteo.arso.gov.si/uploads/probase/www/fproduct/text/sl/forecast_' . $region_id . 'latest.xml';

    $request = $this->sendUrlRequest($url);
    if ($request !== FALSE) {
      return $this->parseResponse($request);
    }

    return FALSE;
  }

  /**
   * Get list of regions.
   *
   */
  private function getRegions() {
    $url = 'https://meteo.arso.gov.si/uploads/probase/www/fproduct/text/sl/forecast_si_latest.xml';
    $request = $this->sendUrlRequest($url);

    if ($request !== FALSE) {
      return $this->parseLocation($request);
    }

    return FALSE;
  }

  /**
   * Get list of cities.
   *
   */
  private function getCities() {
    $url = 'https://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observationAms_si_latest.xml';
    $request = $this->sendUrlRequest($url);

    if ($request !== FALSE) {
      return $this->parseLocation($request);
    }

    return FALSE;
  }

  /**
   * Get water temperature.
   */
  private function getHidroLocation() {
    $url = 'https://www.arso.gov.si/xml/vode/hidro_podatki_zadnji.xml';
    $request = $this->sendUrlRequest($url);

    if ($request !== FALSE) {
      return $this->parseLocation($request);
    }

    return FALSE;
  }

  /**
   * Get water temperature.
   */
  private function getHidroData($hidro_station) {
    $url = 'https://www.arso.gov.si/xml/vode/hidro_podatki_zadnji.xml';
    $request = $this->sendUrlRequest($url);

    if (!is_null($request) && $request !== FALSE) {
      $xml = new SimpleXMLElement($request, NULL, FALSE);

      // Hidro location.
      if (isset($xml->postaja)) {
        // Loop over all data.
        foreach ($xml->postaja as $hidro_location) {
          // Cast SimpleXMLElement to string.
          if ($hidro_location->merilno_mesto == $hidro_station) {
            return (string) $hidro_location->temp_vode;
          }
        }
      }
    }
    return NULL;
  }


  /**
   * Get water temperature.
   */
  private function getSnowLocations() {
    $url = 'https://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_si-snow_latest.html';

    $dom = HtmlDomParser::file_get_html($url);
    $snow_array = [];
    $locations = [];
    foreach ($dom->find('table tbody tr') as $row) {
      $location = reset($row->find('td.meteoSI-th'))->innertext();
      $snow_conditions = reset($row->find('td.snow_snow_unit'))->innertext();
      $snow_conditions = str_replace('&nbsp;', '0 cm', $snow_conditions);
      $locations[$location] = $location;
      $snow_array[$location] = $snow_conditions;
    }

    $store = \Drupal::service('tempstore.private')->get('weather');
    $store->set('snow_conditions', $snow_array);

    ksort($locations);

    return $locations;
  }

  /**
   * Get snow conditions.
   */
  private function getSnowConditions($city) {
    $store = \Drupal::service('tempstore.private')->get('weather');
    $snow = $store->get('snow_conditions');

    return $snow[$city];
  }


  /**
   * Return current temperature for chosen city.
   *
   * @param array $city
   *
   * @return string
   */
  private function getCurrentTemperature($city) {
    return round($city["temperature"]["t"]) . $city["temperature"]["t_var_unit"];
  }

  /**
   * Get correct icon.
   */
  public function iconsMapping($icon) {
    $nn_icon = reset($icon["nn_icon"]);
    $wwsyn_icon = reset($icon["wwsyn_icon"]);

    if ($wwsyn_icon) {
      return $nn_icon . '_' . $wwsyn_icon;
    }

    return $nn_icon;
  }


  /**
   * @inheritDoc
   */
  public function getCacheMaxAge() {
    // Cache block for 3h.
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $current = $this->currentWeather($this->configuration["weather_city"]);

    // Get extended forecast.
    $extended = $this->extendedWeatherForecast($this->configuration["weather_region"]);

    if ($current !== FALSE && !$extended !== FALSE) {
      // Get only first element.
      $current = array_pop($current);

      if (sizeof($extended) == 6) {
        // Get next 3 days.
        $extended_array = array_slice($extended, 1, 3);

        // Get today's forecast.
        $current_forecast = array_slice($extended, 0, 1);
      }
      else {
        // Get next 3 days.
        $extended_array = array_slice($extended, 1, 3);

        // Get today's forecast.
        $current_forecast = array_slice($extended, 0, 1);
      }

      // Get celsius sign.
      $temp_unit = $current_forecast[0]["temperature"]["tnsyn_var_unit"];

      $data_footer = '';
      $data_icon = '';

      switch ($config["weather_footer_data_options"]) {
        case 'temperature':
          $city = $this->currentWeather($config["weather_footer_temperature"]);
          $city = array_pop($city);
          $data_footer = $this->getCurrentTemperature($city);
          $data_icon = 'temperature-icon';
          break;
        case 'water':
          $data_footer = round($this->getHidroData($config["weather_footer_hidro"])) . $temp_unit;
          $data_icon = 'water-temperature';
          break;
        case 'snow':
          $data_footer = $this->getSnowConditions($config["weather_footer_snow"]);
          $data_icon = 'snow-blanket';
          break;
        default:
          NULL;
      }

      // Current weather settings.
      $current_day = explode(' ', $current["valid_day"]);
      $current_date = explode(' ', $current["valid_date"]);
      $date_timestamp = strtotime(reset($current_date));

      $current_output = [
        '#theme' => 'weather_forecast',
        '#icon' => $this->iconsMapping($current_forecast[0]["weather_icon"]),
        '#current_date' => reset($current_day) . ', ' . date('d. m. Y', $date_timestamp),
        '#current_temperature' => $this->getCurrentTemperature($current),
        '#min_temperature' => $current_forecast[0]["temperature"]["tnsyn"] . $temp_unit,
        '#max_temperature' => $current_forecast[0]["temperature"]["txsyn"] . $temp_unit,
      ];

      foreach ($extended_array as $item) {
        $day = explode(' ', $item["valid_day"]);
        $extended_output[] = [
          '#theme' => 'weather_extended_forecast',
          '#icon' => $this->iconsMapping($item["weather_icon"]),
          '#current_date' => reset($day),
          '#min_temperature' => $item["temperature"]["tnsyn"] . $temp_unit,
          '#max_temperature' => $item["temperature"]["txsyn"] . $temp_unit,
        ];
      }

      // Load image file.
      if (!empty($config['image'][0])) {
        $file = File::load($config['image'][0]);
        $image = file_create_url($file->getFileUri());
      }

      $footer_output = [
        '#theme' => 'weather_footer',
        '#text' => $config["weather_footer_text"],
        '#icon' => $data_icon,
        '#data' => $data_footer,
        '#image' => isset($image) ? $image : NULL,
        '#link' => $config["image_url"],
      ];

      return [
        $current_output,
        [
          '#type' => 'container',
          '#attributes' => ['class' => ['extended-forecast-wrapper']],
          '#children' => [
            $extended_output,
          ],
        ],
        $footer_output,
      ];
    }
    else {
      return NULL;
    }
  }
}
